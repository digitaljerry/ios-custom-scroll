//
//  LGestureView.h
//  UIScrollView-Add-Custom-PanGestures
//
//  Created by Jernej Zorec on 08/06/15.
//  Copyright (c) 2015 George Poenaru. All rights reserved.
//

#import <UIKit/UIKit.h>

@class LGestureView;

@protocol LGestureViewDelegate <NSObject>

- (void)didPanVertical:(CGFloat)diff;
- (void)didPanHorizontal:(CGFloat)diff;

@end


@interface LGestureView : UIView  <UIGestureRecognizerDelegate>

@property(nonatomic) UILabel *label;
@property(nonatomic) NSMutableArray *accumulator;
@property(nonatomic) UIScrollView *mainScrollView;

@property (nonatomic, weak) id <LGestureViewDelegate> delegate;

@end
