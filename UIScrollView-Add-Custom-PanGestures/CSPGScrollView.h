//
//  CSPGScrollView.h
//  UIScrollView-Add-Custom-PanGestures
//
//  Created by George Poenaru on 3/29/13.
//  Copyright (c) 2013 George Poenaru. All rights reserved.
//

#import <UIKit/UIKit.h>

@class CSPGScrollView;

@protocol CSPGScrollViewDelegate <NSObject>

@required
- (void)scrollViewDidScrollHorizontal:(CGFloat)offset;
- (void)scrollViewDidScrollVertical:(CGFloat)offset;

@end

@interface CSPGScrollView : UIScrollView <UIGestureRecognizerDelegate>

//don't forget to conform to the UIGestureRecognizerDelegate protocol to be able to recognize self.panGestureRecognition and your verticalPan gesture in the same time

@property (nonatomic, weak) id <CSPGScrollViewDelegate> scrollDirectionDelegate;

@property(nonatomic) UILabel *label;
@property(nonatomic) NSMutableArray *accumulator;
@property(nonatomic) BOOL horizontal;

@end
