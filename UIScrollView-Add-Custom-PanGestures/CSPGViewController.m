//
//  CSPGViewController.m
//  UIScrollView-Add-Custom-PanGestures
//
//  Created by George Poenaru on 3/29/13.
//  Copyright (c) 2013 George Poenaru. All rights reserved.
//

#import "CSPGViewController.h"
#import "CSPGScrollView.h"
#import "LGestureView.h"

@interface CSPGViewController () <CSPGScrollViewDelegate>

@end

@implementation CSPGViewController {
    UIScrollView  *mainScrollView;
    UIScrollView  *secondScrollView;
    CSPGScrollView  *customScrollView;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    customScrollView = [[CSPGScrollView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width,
                                                                       self.view.frame.size.height)];
    customScrollView.scrollDirectionDelegate = self;
    customScrollView.bounces = YES;
    customScrollView.directionalLockEnabled = YES;
    customScrollView.pagingEnabled = YES;
    
    mainScrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width,
                                                                                         self.view.frame.size.height)];
    [mainScrollView setBackgroundColor:[UIColor grayColor]];
    mainScrollView.scrollEnabled = NO;
    mainScrollView.bounces = NO;
    
    UIImageView *image = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width*4, self.view.frame.size.height)];
    //image.contentMode = UIViewContentModeScaleAspectFit;
    [image setImage:[UIImage imageNamed:@"high-resolution-wallpapers-25.jpg"]];
    
    [mainScrollView addSubview:image];
    
    [mainScrollView setContentSize:image.frame.size];
    mainScrollView.alpha = 0.5;
    
    
    ///
    secondScrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width,
                                                                    self.view.frame.size.height)];
    secondScrollView.scrollEnabled = NO;
    
    UIImageView *image2 = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height*3)];;
    [image2 setImage:[UIImage imageNamed:@"castle-how-about-some-tall-walls-got-few-high-resolution-desktop-1200x1920-wantedwallpapers-4456411.jpg"]];
    image2.contentMode = UIViewContentModeRight;
    [secondScrollView addSubview:image2];
    
    [secondScrollView setContentSize:image.frame.size];
    
    [customScrollView setContentSize:CGSizeMake(image.frame.size.width, image2.frame.size.height)];
    
    [self.view addSubview:secondScrollView];
    [self.view addSubview:mainScrollView];
    [self.view addSubview:customScrollView];
//    [self.view addSubview:customView];
    
    //Add Label
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(self.view.frame)/2, CGRectGetHeight(self.view.frame)/10)];
    [label setCenter:CGPointMake(CGRectGetMidX(self.view.frame), CGRectGetMidY(self.view.frame))];
    [label setBackgroundColor:[UIColor greenColor]];
    [label setTextAlignment:NSTextAlignmentCenter];
    [label setText:@" NSLog:"];
    [label setTextColor:[UIColor redColor]];
    label.hidden = YES;
    [self.view addSubview:label];
    
    customScrollView.label = label;
//    customView.label = label;
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)scrollViewDidScrollHorizontal:(CGFloat)offset {
    [mainScrollView setContentOffset:CGPointMake(offset, mainScrollView.contentOffset.y)];
}

- (void)scrollViewDidScrollVertical:(CGFloat)offset {
    [secondScrollView setContentOffset:CGPointMake(secondScrollView.contentOffset.x, offset)];
}

//
//- (void)didPanVertical:(CGFloat)diff {
//    NSLog(@"vertical");
//    [mainScrollView setContentOffset:CGPointMake(mainScrollView.contentOffset.x, mainScrollView.contentOffset.y+diff)];
//}
//
//- (void)didPanHorizontal:(CGFloat)diff {
//    NSLog(@"horizontal");
//    [mainScrollView setContentOffset:CGPointMake(mainScrollView.contentOffset.x + diff, mainScrollView.contentOffset.y)];
//}

@end
